RM= rm -rf
CC=g++

LIB_DIR=./lib
INC_DIR=./include
SRC_DIR=./src
OBJ_DIR=./build
BIN_DIR=./bin
DOC_DIR=./doc
TEST_DIR=./test

CFLAGS= -Wall -pedantic -ansi -std=c++11 -I. -I$(INC_DIR)

.PHONY: all clean debug doc doxy

all: programa1 programa2 #programa3

debug: CFLAGS += -g -O0
debug: programa1 programa2 #programa3

programa1: CFLAGS += -I$(INC_DIR)/programa1

programa1: $(OBJ_DIR)/programa1/main.o
	@echo "============="
	@echo "Ligando o alvo $@"
	@echo "============="
	$(CC) $(CFLAGS)/programa1 -o $(BIN_DIR)/$@ $^
	@echo "+++ [Executavel 'programa1' criado em $(BIN_DIR)] +++"
	@echo "============="	

$(OBJ_DIR)/programa1/main.o: $(SRC_DIR)/programa1/main.cpp
	$(CC) -c $(CFLAGS) -o $@ $<

programa2: CFLAGS += -I$(INC_DIR)/programa2

programa2: $(OBJ_DIR)/programa2/main.o
	@echo "============="
	@echo "Ligando o alvo $@"
	@echo "============="
	$(CC) $(CFLAGS)/programa2 -o $(BIN_DIR)/$@ $^
	@echo "+++ [Executavel 'programa2' criado em $(BIN_DIR)] +++"
	@echo "============="	

$(OBJ_DIR)/programa2/main.o: $(SRC_DIR)/programa2/main.cpp
	$(CC) -c $(CFLAGS) -o $@ $<

#programa3: CFLAGS += -I$(INC_DIR)/programa3

#programa3: $(OBJ_DIR)/programa3/main.o
	#@echo "============="
	#@echo "Ligando o alvo $@"
	#@echo "============="
	#$(CC) $(CFLAGS)/programa3 -o $(BIN_DIR)/$@ $^
	#@echo "+++ [Executavel 'programa3' criado em $(BIN_DIR)] +++"
	#@echo "============="	

#$(OBJ_DIR)/programa3/main.o: $(SRC_DIR)/programa3/main.cpp
	#$(CC) -c $(CFLAGS) -o $@ $<

doxy: 
	doxygen -g

doc:
	$(RM) $(DOC_DIR)/*
	doxygen	


clean:
	$(RM) $(DOC_DIR)/*
	$(RM) $(OBJ_DIR)/*