/**
 * @file	palindromo.h
 * @brief	Definiçao da classe Stack com template, que representa uma pilha e suas açoes . 
 * @author	Cleydson Talles Araujo Vieira
 * @since	11/05/2017
 * @date	21/05/2017 
*/



#ifndef PALINDROMO_H
#define PALINDROMO_H

#include <iostream>
using std::cout ;
using std::endl ;
using std::cin ;
using std::cerr ;

#include <string>
using std::string ;

/**
 * @class Stack palindromo.h
 * @brief Classe que representa uma pilha
*/
template<class T>
class Stack {
	private :
		T *elementos ;
		int tamanho ;
		int max ;
	public :
		Stack(int m) ;
		~Stack () ;
		/** @brief empilha elemento */
		void push (T x) ;
		/** @brief retira um elemento */
		void pop () ;
		/** @brief retorna o ultimo elemento da pilha */
		T top () ;
		/** @brief mostra todos os elementos da pilha */		
		void mostra_vetor () ;



} ;
/**
 * @param m Tamanho da pilha
*/
template<class T>
Stack<T>::Stack (int m) {
	if (m > 0) {
		max = m ;
		tamanho = 0 ;
		elementos = new T[m] ;
	}
	else {
		cerr << " valor invalido " << endl ;
		exit (1) ;
	}	
}

template<class T>
Stack<T>::~Stack() {
	delete[] elementos ;
} 
/**
 * @brief empilha elemento
 * @param x Elemento a ser inserido na pilha
*/
template<class T>
void Stack<T>::push (T x) {
	elementos[tamanho] = x ;
	tamanho++ ;
}

/**
 * @brief Retira um elemento
*/
template<class T>
void Stack<T>::pop () {
	if (tamanho < 0) {
		cout << "a pilha esta vazia" << endl ;
	}
	else {
		tamanho-- ;
	}	
}
/**
 * @return Retorna o ultimo elemento da pilha
*/
template<class T>
T Stack<T>::top () {
	return elementos[tamanho-1] ;
}
/**
 * @brief Lista todos os elementos da pilha
*/
template<class T>
void Stack<T>::mostra_vetor () {
	for (int i  = 0 ; i <= tamanho ; i++) {
		cout << elementos[i] << " " ;
	}
	cout <<"\n" ;
}





#endif /* PALINDROMO_H */