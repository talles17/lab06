


#ifndef LISTA_H
#define LISTA_H

#include <iostream>
using std::cout ;
using std::cin ;
using std::endl ;
using std::cerr ;


template<class T>
class Lista {
	private :
		int tamanho ;
		struct Node {
			T dado ;
			Node *prox ;
			Node *ante ;
		} ; 
		Node *head ;
		Node *curr ;
		Node *temp ;
		//Node *iter = node ;
		

	public :

		Lista() ;
		~Lista() ;
		void preencheLista (T x) ;
		void add_inicio (T x) ;
		void remocaoLista (T delDado) ;
		void mostrarLista () ;
		T acessoLista (Node node , int pos) ;

} ;

template<typename T>
Lista<T>::Lista () {
	head = NULL ;
	curr = NULL ;
	temp = NULL ;


}

template<typename T>
Lista<T>::~Lista () {
	delete [] head ;
	delete [] curr ;
	delete [] temp ;
}

template<typename T>
void Lista<T>::preencheLista (T x) {
	
	temp->prox = NULL ;
	temp->dado = x ;

	if (head != NULL ) {
		curr = head ;
		while (curr->prox != NULL) {
			
			curr = curr->prox ;

		}
		curr->prox = temp  ; 
		temp->ante = curr ;
	}
	else {
		temp->ante = NULL ;
		head = temp ;
	}
}

template<typename T>
void Lista<T>::add_inicio (T x) {
	if (head == NULL) {
		cerr << " A lista nao foi criada" << endl ;
		return ;
	}
	else {
		temp->ante = NULL ;
		temp->dado = x ;
		temp->prox = head ;
		head->ante = temp ;
		head = temp ;
		cout << "Elemento " << x << " inserido" << endl ;
	}

}


template<typename T>
void Lista<T>::remocaoLista (T delDado) {
	Node remocao = NULL ;

}

template<typename T>
void Lista<T>::mostrarLista () {
	curr = head ;
	if (head == NULL) {
		cout << "Lista vazia" << endl ;
		return ;
	}
	while (curr != NULL) {
		cout << curr->dado << endl ; ;
		curr = curr->prox ;
	}

}

template<typename T>
T Lista<T>::acessoLista (Node head, int pos) {
	if (head == NULL) {
		cerr << "A lista esta vazia." << endl ;
		exit (1) ;
	}
	else {
		Node *iter = head ;
		for (int i = 0 ; i < pos ; i++) {
			iter = iter->prox ;
			if (iter == NULL) {
				cerr << "Erro" << endl ;
				exit (1) ;
			}
			return iter->dado ;
		}
	} 


}



#endif /* LISTA_H */
