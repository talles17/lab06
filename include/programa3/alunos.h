#ifndef ALUNOS_H
#define ALUNOS_H

#include <ostream>
using std::ostream ;
#include <istream>
using std::istream ;

#include <string>
using std::string ;



class Aluno {
	private :
		string matricula ;
		string nome ;
		float nota ;
		int faltas ;

	public :
		Aluno () ;
		Aluno(string m , string n , float no , int f) ;
		~Aluno() ;
		void setMatricula (string m) ;
		string getMatricula () ; 
		void setNome (string n) ;
		string getNome () ;
		void setNota (float no) ;
		float getNota () ;
		void setFaltas (int f) ;
		int getFaltas () ;
		friend ostream& operator<<(ostream& os, Aluno &a) ;
		friend istream& operator>>(istream& is, Aluno &a) ;
} ;

Aluno::Aluno () {

} 

Aluno::Aluno (string m , string n , float no , int f) {
	matricula = m ; 
	nome = n ;
	nota = no ;
	faltas = f ;
}

Aluno::~Aluno () {

}

void Aluno::setMatricula (string m) {
	matricula = m ;
}

string Aluno::getMatricula (){
	return matricula ;
}

void Aluno::setNome (string n) {
	nome = n ;
}

string Aluno::getNome () {
	return nome ;
}

void Aluno::setNota(float no) {
	nota = no ;
}

float Aluno::getNota () {
	return nota ;
}

void Aluno::setFaltas (int f) {
	faltas = f ;
}

int Aluno::getFaltas () {
	return faltas ;
}

ostream& operator<<(ostream& os, Aluno &a) {
	os << a.matricula << " " << a.nome << " " << a.nota << " " << a.faltas ;
	return os ;
}

istream& operator>>(istream& is , Aluno &a) {
	is >> a.matricula >> a.nome >> a.nota >> a.faltas ;
	return is ;
}


#endif /* ALUNOS_H */