#ifndef TURMAS_H
#define TURMAS_H

#include <iostream>
using std::cout ;
using std::endl ; 

#include <string>
using std::string ;

#include "alunos.h"

class Turmas {
	private :
		string nomeDaTurma ;
		int quantidadeAlunos ;
		Aluno* alunos ;

	public :
		Turmas () ;
		Turmas(string n) ;
		~Turmas () ;
		int getQuantidadeAlunos () ;
		void addAluno (Aluno a) ;
		void mostraAlunos () ;
		void numeroDeAlunos () ;
		void mediaDosAlunos () ;

} ;

Turmas::Turmas () {
	quantidadeAlunos = 0 ;
}

Turmas::Turmas (string n ) {
	nomeDaTurma = n ;
}

Turmas::~Turmas () {

}

int Turmas::getQuantidadeAlunos () {
	return quantidadeAlunos ;
}

void Turmas::addAluno (Aluno a) {
	quantidadeAlunos++ ;
	Aluno *al ;
	al = new Aluno[quantidadeAlunos] ;
	for (int i = 0 ; i < (quantidadeAlunos-1) ; i++) {
		al = alunos ;
	}
	al[quantidadeAlunos] = &a ;
	if (quantidadeAlunos > 1) {
		delete [] *alunos ;
	}

	alunos = a ;

}

void Turmas::mostraAlunos () {
	cout << nomeDaTurma << endl ;
	for (int i = 0 ; i < quantidadeAlunos ; i++) {
		cout << alunos->getNome << endl ; 
	} 
}

void Turmas::numeroDeAlunos () {
	cout << "A quantidade de alunos da turma " << nomeDaTurma << " e " << quantidadeAlunos << endl ;
}

void Turmas::mediaDosAlunos () {
	float soma = 0 ;
	for (int i  = 0 ; i < quantidadeAlunos ; i++) {
		soma += alunos->getNota ;
	}
	cout << "A media da turma " << nomeDaTurma << " e " << soma/quantidadeAlunos ;
}


#endif /* TURMAS_H */ 