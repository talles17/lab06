/**
 * @file	main.cpp
 * @brief	Codigo fonte principal do programa 1 
 * @author	Cleydson Talles Araujo Vieira
 * @since	11/05/2017
 * @date	21/05/2017 
 * @sa 		"palindromo.h"
*/

#include <iostream>
using std::cout ;
using std::endl ;
using std::cin ;

#include <string>
#include <cstring>

#include "palindromo.h"

/** @brief funcao principal */
int main () {
	int n ;
	
	cout << "Digite o tamanho do vetor da pilha: " ;
	cin >> n ;
	
	char *frase = new char[n] ;
	
	Stack<char> pilha(n) ;
	
	cout << "Digite a frase: " ;
	cin >> frase ;
	

	int tamanho = strlen(frase) ;

	for (int i  = 0 ; i < tamanho ; i++) {
		pilha.push(frase[i]) ;
	}

	int j = 0 ;



	while (j < (tamanho/2) ) {
		if (pilha.top() != frase[j]) {
			cout << "A frase nao e um palindromo." << endl ;
			break ;

		}
		else {
			j++ ;
			pilha.pop() ;
		}
	}

	 if (j == (tamanho/2) ) {
	 	cout << "A frase e um palindromo" << endl ;
	 }



	

	return 0 ;


}